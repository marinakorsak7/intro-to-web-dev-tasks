import boxing from "../../assets/images/boxing.png";

export default function Additional() {
  return `
    <section class="additional">
      <img src=${boxing} alt="Box" class="additional-img">
      <div>
        <h2 class="additional_title">
          Whatever you get in the box
        </h2>
        <ul class="additional-contains">
          <li class="additional_item">
            5A Charger
          </li>
          <li class="additional_item">
            Extra battery
          </li>
          <li class="additional_item">
            Sophisticated bag
          </li>
          <li class="additional_item">
            User manual guide
          </li>
        </ul>
      </div>
    </section>
  `;
}
