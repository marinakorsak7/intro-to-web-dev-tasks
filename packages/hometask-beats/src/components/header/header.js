import logo from "../../assets/icons/logo.svg";
import search from "../../assets/icons/search.svg";
import box from "../../assets/icons/box.svg";
import user from "../../assets/icons/user.svg";
import Hero from "../hero/hero";

export default function Header() {
  return `
  <div class="container hero-container">  
   <header>
      <nav class="header">
        <img src=${logo} alt="Logotype" class="logo" />
      
        <ul class="header_items">
          <li class="header_item">
            <img
              src=${search}
              alt="Search"
              class="header_item_img"
            />
          </li>
          <span class="header_items_divider">&#124;</span>
          <li class="header_item">
            <img
              src=${box}
              alt="Shop"
              class="header_item_img"
            />
          </li>
          <span class="header_items_divider">&#124;</span>
          <li class="header_item">
            <img
              src=${user}
              alt="User"
              class="header_item_img"
            />
          </li>
        </ul>

        <div class="header_burger">
          <div class="burger-line big-line"></div>
          <div class="burger-line small-line"></div>
          <div class="burger-line big-line"></div>
          <div class="burger-line small-line"></div>
        </div>
      </nav>
    </header>
    ${Hero()}
  </div>
  `;
}
