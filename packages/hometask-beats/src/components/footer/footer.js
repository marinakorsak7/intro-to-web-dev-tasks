import logo from "../../assets/icons/logo.svg";
import instagram from "../../assets/icons/instagram.svg";
import twitter from "../../assets/icons/twitter.svg";
import facebook from "../../assets/icons/facebook.svg";

export default function Footer() {
  return `
    <footer class="footer">
      <img src=${logo} alt="Beats" class="footer_link_img">
      <ul class="footer_navigation">
        <li class="nav-item">
          Home
        </li>
        <li class="nav-item">
          About
        </li>
        <li class="nav-item">
          Product
        </li>
      </ul>

      <ul class="footer_links">
        <li class="footer_link">
          <img src=${instagram} alt="Instagram" class="footer_link_img">
        </li>
        <li class="footer_link">
          <img src=${twitter} alt="Twitter" class="footer_link_img">
        </li>
        <li class="footer_link">
          <img src=${facebook} alt="Facebook" class="footer_link_img">
        </li>
      </ul>
    </footer>
  `;
}
