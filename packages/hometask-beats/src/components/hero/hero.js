import hero from "../../assets/images/hero.png";

export default function Hero() {
  return `
   <main class="hero">
    <img src=${hero} alt="Headphones" class="hero-img">
    <div class="hero_content">
      <h2 class="hero_subtitle">
        Hear it. Feel it
      </h2>
      <h1 class="hero_title">Move with the music</h1>
      <div class="hero_price">
        <div class="price-actual">$ 435</div>
        <div class="price-divider">&#124;</div>
        <div class="price-old">$ 465</div>
      </div>
  
      <button class="hero_buy-btn">
        BUY NOW
      </button>
    </div>
   </main>
  `;
}
