import product3 from "../../assets/images/product-03.png";
import product2 from "../../assets/images/product-02.png";
import product1 from "../../assets/images/product-01.png";
import star from "../../assets/icons/star.svg";

export default function Shop() {
  return `
    <section class="shop">
      <h2 class="shop_title">
        Our Latest Product
      </h2>
      <p class="shop_descr">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis nunc ipsum aliquam, ante.
      </p>

      <div class="shop-cards">
        <div class="shop-card card-red inactive">
          <img src=${product3} alt="Product" class="product_img">
          <div class="rating_wrapper">
            <ul class="rating_stars">
              <li class="rating_star">
               <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
               <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
               <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
               <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
               <img src=${star} alt="star" class="star_img">
              </li>
            </ul>
            <span class="rating_number">4.50</span>
          </div>
          <div class="card_info">
            <span class="card_title">
              Red Headphone
            </span>
            <span class="card_price">$ 235</span>
          </div>
        </div>
        <div class="shop-card">
          <img src=${product2} alt="Product" class="product_img">
          <div class="rating_wrapper">
            <ul class="rating_stars">
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
            </ul>
            <span class="rating_number">4.50</span>
          </div>
          <div class="card_info">
            <span class="card_title">
              Blue Headphone
            </span>
            <span class="card_price">$ 235</span>
          </div>
        </div>
        <div class="shop-card card-green inactive">
          <img src=${product1} alt="Product" class="product_img">
          <div class="rating_wrapper">
            <ul class="rating_stars">
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
              <li class="rating_star">
                <img src=${star} alt="star" class="star_img">
              </li>
            </ul>
            <span class="rating_number">4.50</span>
          </div>
          <div class="card_info">
            <span class="card_title">
              Green Headphone
            </span>
            <span class="card_price">$ 235</span>
          </div>
        </div>
      </div>
    </section>
  `;
}
