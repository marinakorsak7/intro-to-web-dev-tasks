import battery from "../../assets/icons/battery.svg";
import bluetooth from "../../assets/icons/bluetooth.svg";
import microphone from "../../assets/icons/microphone.svg";
import feature from "../../assets/images/feature.png";

export default function Specification() {
  return `
    <section>
      <div class="product-specification">
        <div>
          <h2 class="specification-title">
            Good headphones and loud music is all you need
          </h2>

          <div class="specification_container">
            <div class="specification_icon">
              <img src=${battery} alt="Battery" class="specification_icon">
            </div>
            <div class="item-specification">
              <h3 class="specification-item_title">
                Battery
              </h3>
              <div class="specification_info">
                Battery 6.2V-AAC codec
              </div>
              <a href="#" class="specification_learn">Learn More</a>
            </div>
          </div>

          <div class="specification_container">
            <div class="specification_icon">
              <img src=${bluetooth} alt="Bluetooth" class="specification_icon">
            </div>
            <div class="item-specification">
              <h3 class="specification-item_title">
                Bluetooth
              </h3>
              <div class="specification_info">
                Battery 6.2V-AAC codec
              </div>
              <a href="#" class="specification_learn">Learn More</a>
            </div>
          </div>

          <div class="specification_container">
            <div class="specification_icon">
              <img src=${microphone} alt="microphone" class="specification_icon">
            </div>
            <div class="item-specification">
              <h3 class="specification-item_title">
                Microphone
              </h3>
              <div class="specification_info">
                Battery 6.2V-AAC codec
              </div>
              <a href="#" class="specification_learn">Learn More</a>
            </div>
          </div>
        </div>
        <img src=${feature} alt="Feature" class="specification-photo">
      </div>
    </section>
  `;
}
