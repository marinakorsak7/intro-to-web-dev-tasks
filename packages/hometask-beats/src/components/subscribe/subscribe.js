export default function SubscribeForm() {
  return `
    <section class="subscribe">
      <h2 class="subscribe_title">
        Subscribe
      </h2>
      <p class="subscribe_text">
        Lorem ipsum dolor sit amet, consectetur
      </p>
      <div class="subscribe_input_wrapper">
        <input type="text" class="subscribe_input" placeholder="Enter Your email address">
        <button class="subscribe_btn">
          Subscribe
        </button>
      </div>
    </section>
  `;
}
