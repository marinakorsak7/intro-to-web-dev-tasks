import arrowLeft from "../../assets/icons/arrow-left.svg";
import clour2 from "../../assets/images/clour-02.png";
import clour1 from "../../assets/images/clour-01.png";
import clour3 from "../../assets/images/clour-03.png";
import arrowRight from "../../assets/icons/arrow-right.svg";

export default function Products() {
  return `
    <section class="products">
      <h2 class="products-title">
        Our Latest
        <br>
        colour collection 2021
      </h2>
      <div class="products-carousel">
        <img src=${arrowLeft} alt="Left arrow" class="carousel_arrow arrow-left">
        <div class="carousel-card inactive">
          <img src=${clour2} alt="" class="carousel-card_img">
        </div>
        <div class="carousel-card active">
          <img src=${clour1} alt="" class="carousel-card_img">
        </div>
        <div class="carousel-card inactive">
          <img src=${clour3} alt="" class="carousel-card_img">
        </div>
        <img src=${arrowRight} alt="Right arrow" class="carousel_arrow arrow-right">
      </div>
    </section>
  `;
}
