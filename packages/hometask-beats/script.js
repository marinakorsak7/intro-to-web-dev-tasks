import Header from "./src/components/header/header";
import Products from "./src/components/products/products";
import Specification from "./src/components/specification/specification";
import Shop from "./src/components/shop/shop";
import Additional from "./src/components/additional/additional";
import SubscribeForm from "./src/components/subscribe/subscribe";
import Footer from "./src/components/footer/footer";

const App = document.querySelector(`.app`);

App.innerHTML =
  Header() +
  Products() +
  Specification() +
  Shop() +
  Additional() +
  SubscribeForm() +
  Footer();
