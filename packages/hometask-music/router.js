import Header from "./src/components/header/header";
import Footer from "./src/components/footer/footer";
import Hero from "./src/components/hero/hero";
import Discover from "./src/components/discover/discover";
import SignUp from "./src/components/signup/signup";

const App = document.querySelector(`.app`);

App.innerHTML = Header() + Hero() + Footer();

App.addEventListener("click", (event) => {
  const { target } = event;

  if (target.classList.contains("discover")) {
    App.innerHTML = Header() + Discover() + Footer();
  }

  if (target.classList.contains("join")) {
    App.innerHTML = Header() + SignUp() + Footer();
  }

  if (target.classList.contains("hero")) {
    App.innerHTML = Header() + Hero() + Footer();
  }
});
