import Button from "../button/button";

export default function Hero() {
  return `
   <section class="hero-section">
      <div class="hero-content">
        <h1 class="hero-heading">Feel the music</h1>
        <p class="hero-description">Stream over 10 million songs with one click</p>
        ${Button("Join Now", "join")}
      </div>
      <div class="hero-img"></div>
   </section>
  `;
}
