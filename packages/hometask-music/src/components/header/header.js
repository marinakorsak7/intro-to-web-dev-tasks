import logo from "../../assets/icons/logo.svg";

export default function Header() {
  return `
    <header class="container header-container">
      <nav class="header">
        <a href="#" class="header-logo">
          <img src=${logo} alt="Logotype" class="logo" />
        </a>
      
        <ul class="header-items">
          <li class="header-list-item hero">Simo</li>
          <li class="header-list-item discover">Discover</li>
          <li class="header-list-item join">Join</li>
          <li class="header-list-item" id="sign-up">Sign In</li>
        </ul>

      </nav>
    </header>
  `;
}
