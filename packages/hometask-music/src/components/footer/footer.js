export default function Footer() {
  return `
    <footer class="container footer">
      <ul class="footer-navigation">
        <li class="footer-nav-item">
          About Us
        </li>
        <li class="footer-nav-item">
          Contact
        </li>
        <li class="footer-nav-item">
          Cr Info
        </li>
        <li class="footer-nav-item">
          Twitter
        </li>
        <li class="footer-nav-item">
          Facebook
        </li>
      </ul>

      <div class="footer-copyright">Wlodzimierz Simon © 2022</div>
    </footer>
  `;
}
