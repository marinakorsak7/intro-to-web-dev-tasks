export default function Button(text, className) {
  return `
    <button class='button ${className}'>
        ${text}
    </button>
  `;
}
