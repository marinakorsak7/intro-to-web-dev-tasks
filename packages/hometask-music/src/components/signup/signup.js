import Button from "../button/button";

export default function SignUp() {
  return `
    <section class='signup'>
      <form class='signup-form'>
        <div class='signup-input-wrapper'>
          <label for="username" class='signup-input-label'>Name:</label>
          <input id="username" name="username" type="text" class='signup-form-input'/> 
        </div>
        <div class='signup-input-wrapper'>
          <label for="password" class='signup-input-label'>Password:</label>
          <input id="password" name="password" type="text" class='signup-form-input'/> 
        </div>
        <div class='signup-input-wrapper'>
          <label for="e-mail" class='signup-input-label'>e-mail:</label>
          <input id="e-mail" name="e-mail:" type="text" class='signup-form-input'/> 
        </div>
      </form>
      ${Button("Join Now", "join")}
      <div class="signup-img"></div>
    </section>
  `;
}
