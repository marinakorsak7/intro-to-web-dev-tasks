import Button from "../button/button";

export default function Discover() {
  return `
    <section class='discover-section'>
      <div class='discover-content'>
        <h1 class='discover-heading'>
          Discover new music
        </h1>
        <div class='discover-buttons'>
          ${Button("Charts", "")}
          ${Button("Songs", "")}
          ${Button("Artists", "")}
        </div>
        <p class='discover-text'>
          By joing you can benefit by listening to the latest albums released
        </p>
      </div>
      <div class="discover-img"></div>
    </section>
  `;
}
